lazy val root = (project in file(".")).
settings(
    inThisBuild(List(
      organization := "com.epita",
      scalaVersion := "2.11.6",
      version      := "1.0.0"
    )),
    name := "ScalaProj",
    assemblyOption in assembly ~= {_.copy(includeScala = false)},
    connectInput in run := true,
    fork in run := true,

    dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-core" % "2.6.7",
    dependencyOverrides += "com.fasterxml.jackson.core" % "jackson-databind" % "2.6.7",

    libraryDependencies ++= Seq("org.apache.kafka" % "kafka_2.11" % "1.1.0"
      exclude("javax.jms", "jms")
      exclude("com.sun.jdmk", "jmxtools")
      exclude("com.sun.jmx", "jmxri")
      exclude("com.sun.jmx", "jmxri")
      exclude("com.fasterxml.jackson.core", "jackson-core")
      exclude("org.slf4j", "slf4j-simple")),
      // Logback with slf4j facade
//      "ch.qos.logback" % "logback-classic" % "1.0.13",
//      "ch.qos.logback" % "logback-core" % "1.0.13",
 //     "org.slf4j" % "slf4j-api" % "1.7.5"),
    libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.11.0",


    libraryDependencies += "com.lightbend" %% "kafka-streams-scala" % "0.2.1",
    libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.3.1", //exclude("com.fasterxml.jackson.core", "jackson-core"),
    libraryDependencies += "org.apache.spark" %% "spark-streaming-kafka-0-10" % "2.3.1" exclude("com.fasterxml.jackson.core", "jackson-core"),
    libraryDependencies += "org.springframework.kafka" % "spring-kafka" % "2.1.7.RELEASE" exclude("com.fasterxml.jackson.core", "jackson-core"),
    libraryDependencies += "org.apache.spark" %% "spark-core" % "2.3.1" exclude("com.fasterxml.jackson.core", "jackson-core"),

    scalacOptions := Seq("-unchecked", "-deprecation")
  )

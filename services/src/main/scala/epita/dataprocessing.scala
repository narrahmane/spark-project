package epita

import epita.ModuleSerdes.{CustomSerializer, CustomDeserializer}
import epita.ModuleDatastructures._
import org.apache.kafka.streams.kstream._
import org.apache.kafka.streams._
import org.apache.kafka.common.serialization._
import java.util.Properties
import java.time.Instant
import org.apache.kafka.streams.Topology.AutoOffsetReset._
import scala.collection.JavaConversions._
import java.lang.Long

object DataProcessing {

  def main(args: Array[String]) : Unit = {
    args.length match {
      case x if x < 1 => println("Not enough arguments, missing brokers list")
      case x if x >= 1 => process(args)
    }
  }

  def process(brokers: Array[String]) : Unit = {
    val longSerde = Serdes.serdeFrom(new LongSerializer, new LongDeserializer)
    val stringSerde = Serdes.serdeFrom(new StringSerializer, new StringDeserializer)
    val userSerde = Serdes.serdeFrom(new CustomSerializer[User], new CustomDeserializer[User])
    val postSerde = Serdes.serdeFrom(new CustomSerializer[Post], new CustomDeserializer[Post])
    val messageSerde = Serdes.serdeFrom(new CustomSerializer[Message], new CustomDeserializer[Message])
    val denormalizedPostSerde = Serdes.serdeFrom(new CustomSerializer[DenormalizedPost], new CustomDeserializer[DenormalizedPost])
    val denormalizedMessageSerde = Serdes.serdeFrom(new CustomSerializer[DenormalizedMessage], new CustomDeserializer[DenormalizedMessage])

    val builder = new StreamsBuilder()
    val usersTable: KTable[String, User] = builder.table("topic-users", Consumed.`with`(stringSerde, userSerde))
    val postsStream: KStream[String, Post] = builder.stream("topic-posts", Consumed.`with`(stringSerde, postSerde))
    val messagesStream: KStream[String, Message] = builder.stream("topic-messages", Consumed.`with`(stringSerde, messageSerde))

    val postByUser: KStream[String, DenormalizedPost] =
      postsStream
        .leftJoin[User, DenormalizedPost](usersTable,
                  new ValueJoiner[Post, User, DenormalizedPost]
                      { def apply(post: Post, user: User) =
                            new DenormalizedPost(post, user)
                      },
                  Joined.`with`(stringSerde, postSerde, userSerde))
    postByUser.to("topic-postbyuser", Produced.`with`(stringSerde, denormalizedPostSerde))

    val postByUserStream: KStream[String, DenormalizedPost] = builder.stream("topic-postbyuser", Consumed.`with`(stringSerde, denormalizedPostSerde))

    val timestampByDenormalizedPost =
      postByUserStream
	.map[java.lang.Long, DenormalizedPost](new KeyValueMapper[String, DenormalizedPost, KeyValue[java.lang.Long, DenormalizedPost]]
                                                   { def apply(username: String, denomPost: DenormalizedPost) = {
                                                         new KeyValue(denomPost.post.timestamp, denomPost)
                                                   }
                                              })
        .to("topic-timestampDpost", Produced.`with`(longSerde, denormalizedPostSerde))

    val messageBySender: KStream[String, DenormalizedMessage] =
      messagesStream
        .leftJoin[User, DenormalizedMessage](usersTable,
                 new ValueJoiner[Message, User, DenormalizedMessage]
                     { def apply(msg: Message, user: User) =
                           new DenormalizedMessage(user, null, msg)
                     },
                 Joined.`with`(stringSerde, messageSerde, userSerde))
        .map[String, DenormalizedMessage](new KeyValueMapper[String, DenormalizedMessage, KeyValue[String, DenormalizedMessage]]
                                                         { def apply(username: String, denomMsg: DenormalizedMessage) = {
                                                               new KeyValue(denomMsg.message.receiver, denomMsg)
                                                               }
                                                         })
        .leftJoin[User, DenormalizedMessage](usersTable,
                 new ValueJoiner[DenormalizedMessage, User, DenormalizedMessage]
                     { def apply(denomMsg: DenormalizedMessage, user: User) =
                           denomMsg.copy(receiver=user)
                     },
                 Joined.`with`(stringSerde, denormalizedMessageSerde, userSerde))
        .map[String, DenormalizedMessage](new KeyValueMapper[String, DenormalizedMessage, KeyValue[String, DenormalizedMessage]]
                                                         { def apply(username: String, denomMsg: DenormalizedMessage) = {
                                                               new KeyValue(denomMsg.message.sender, denomMsg)
                                                               }
                                                         })
    messageBySender.to("topic-messagebysender", Produced.`with`(stringSerde, denormalizedMessageSerde))

    val messageBySenderStream: KStream[String, DenormalizedMessage] = builder.stream("topic-messagebysender", Consumed.`with`(stringSerde, denormalizedMessageSerde))

    val timestampByDenormalizedMessage =
      messageBySenderStream
        .map[java.lang.Long, DenormalizedMessage](new KeyValueMapper[String, DenormalizedMessage, KeyValue[java.lang.Long, DenormalizedMessage]]
                                                         { def apply(username: String, denomMsg: DenormalizedMessage) = {
                                                               new KeyValue(denomMsg.message.timestamp, denomMsg)
                                                               }
                                                         })
        .to("topic-timestampDmessage", Produced.`with`(longSerde, denormalizedMessageSerde))

    val config = new Properties()
    val brokerList = brokers.mkString(",")
    println(brokerList)
    config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, brokerList)
    config.put(StreamsConfig.APPLICATION_ID_CONFIG, "DataProcessing")
    val streams = new KafkaStreams(builder.build(), config)
    Runtime.getRuntime.addShutdownHook(new Thread{override def run() = streams.close()})
    streams.start()
  }

}
#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias emacs='emacs -nw'
alias cdsh='cd /tmp/spark-project/services'
alias cdch='cd /tmp/spark-project/client'
alias cdss='cd /tmp/spark-project/services/src/main/scala/epita'
alias cdcs='cd /tmp/spark-project/client/src/main/scala/epita'
alias sbt='/tmp/spark-project/sbt/bin/sbt -java-home /tmp/spark-project/jre1.8.0_171/'

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

package epita

object Main {
  import epita.Action
  import epita.ModuleInput
  import epita.ModuleClient

  def main(args: Array[String]) : Unit = {
    args.length match {
		case x if x < 3 => println("Not enough arguments")
		case x if x >= 3 => val client = ModuleClient.InitializeClient(args)
							interact(client)
	}
  }

  def interact(client: ModuleClient.Client) : Unit = {
    ModuleInput.askForAction() match {
      case Action.SendPost          => client.sendPost(); interact(client)
      case Action.SendMessage       => client.sendMessage(); interact(client)
      case Action.Benchmark         => client.benchmark(); interact(client)
      case Action.BenchmarkPosts    => client.benchmarkPosts(); interact(client)
      case Action.BenchmarkMessages => client.benchmarkMessages(); interact(client)
      case _                        => client.quit()
    }
  }

}

package epita

object ModuleClient {

  import epita.ModuleInput._
  import epita.ModuleDatastructures._
  import epita.ModuleConsumer.ConsumerAPI
  import epita.ModuleProducer.ProducerAPI

  def InitializeClient(args : Array[String]) : Client = {
    val brokerList = args.slice(0, args.length - 2).mkString(",")
    val producer = new ProducerAPI(brokerList, args(args.length - 2))
    val consumer = new ConsumerAPI(args(args.length - 1))

    val user = askForNewUser()
    producer.sendUser(user)
    welcome(user)

    new Client(user, producer, consumer)
  }

  class Client(userInit: User, producerAPI: ProducerAPI, consumerAPI: ConsumerAPI) {
    val user = userInit
    val producer = producerAPI
    val consumer = consumerAPI

    def sendPost() : Unit = {
      val post = askForPost(user)
      this.producer.sendPost(post)
    }

    def sendMessage() : Unit = {
      val message = askForMessage(user)
      this.producer.sendMessage(message)
    }

    def benchmark() : Unit = {
      val dateFrom = askForFilterTime("Date from (YYYY-MM-DD):")
      val dateTo = askForFilterTime("Date to (YYYY-MM-DD):")
      val ageFrom = askForFilterAge("Age from:")
      val ageTo = askForFilterAge("Age to:")
      val brands = askForFilterBrands()

      this.consumer.benchmark(dateFrom, dateTo, ageFrom, ageTo, brands)
    }

    def benchmarkPosts() : Unit = {
      val dateFrom = askForFilterTime("Date from (YYYY-MM-DD):")
      val dateTo = askForFilterTime("Date to (YYYY-MM-DD):")
      val ageFrom = askForFilterAge("Age from:")
      val ageTo = askForFilterAge("Age to:")
      val brands = askForFilterBrands()

      this.consumer.benchmarkPosts(dateFrom, dateTo, ageFrom, ageTo, brands)
    }

    def benchmarkMessages() : Unit = {
      val dateFrom = askForFilterTime("Date from (YYYY-MM-DD):")
      val dateTo = askForFilterTime("Date to (YYYY-MM-DD):")
      val ageFrom = askForFilterAge("Age from:")
      val ageTo = askForFilterAge("Age to:")
      val brands = askForFilterBrands()

      this.consumer.benchmarkMessages(dateFrom, dateTo, ageFrom, ageTo, brands)
    }

    def quit() : Unit = {
      println("\n\nExiting... By")
    }
  }

}
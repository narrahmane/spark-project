package epita

object Action extends Enumeration {
  type Action = Value
  val SendPost,
      SendMessage,
      SendProfile,
      Benchmark,
      BenchmarkPosts,
      BenchmarkMessages,
      Quit = Value
}

object ModuleInput {
  import Action.Action
  import scala.io.StdIn
  import ModuleDatastructures._
  import java.time.format._
  import java.time.{Instant, LocalDate, LocalDateTime, ZoneOffset}
  import java.lang._

  def askForAction() : Action = {
    println("Select an action (1=SendPost, 2=SendMessage, 3=Benchmark, 4=BenchmarkPosts, 5=BenchmarkMessages, 6=Quit):")
    val in = StdIn.readLine()
    in match {
      case "1"  =>  Action.SendPost
      case "2"  =>  Action.SendMessage
      case "3"  =>  Action.Benchmark
      case "4"  =>  Action.BenchmarkPosts
      case "5"  =>  Action.BenchmarkMessages
      case "6"  =>  Action.Quit
      case _    =>  println("\n[ERROR]: Not a valid action\n"); askForAction()
    }
  }

  def askForNewUser() : User = {
    println("Choose a username:")
    val username = StdIn.readLine()
    println("Enter your firstname:")
    val firstname = StdIn.readLine()
    println("Enter your lastname:")
    val lastname = StdIn.readLine()
    println("Enter your age:")
    val age = askForAge()

    User(username, firstname, lastname, age, Instant.now().toEpochMilli())
  }

  def askForAge() : Int = {
    try {
      StdIn.readLine().toInt
    } catch {
      case e: NumberFormatException => println("[ERROR INVALID FORMAT] Enter your age:"); askForAge()
    }
  }

  def askForPost(user: User) : Post = {
    println("What's hapenning?")
    val text = StdIn.readLine()
    Post(user.username, text, Instant.now().toEpochMilli())
  }

  def askForMessage(user: User) : Message = {
    println("Choose receiver's username:")
    val receiver = StdIn.readLine()
    println("Enter some text:")
    val text = StdIn.readLine()
    Message(user.username, receiver, text, Instant.now().toEpochMilli())
  }

  def askForFilterTime(prompt: String) : Long = {
    try {
      println(prompt)
      val localDate = LocalDate.parse(StdIn.readLine());
      val localDateTime = localDate.atStartOfDay();
      val instant = localDateTime.toInstant(ZoneOffset.UTC);
      instant.toEpochMilli()
    } catch {
      case e: DateTimeParseException => print("[ERROR INVALID FORMAT] "); askForFilterTime(prompt)
    }
  }

  def askForFilterAge(prompt: String) : Int = {
    try {
      println(prompt)
      StdIn.readLine().toInt
    } catch {
      case e: NumberFormatException => print("[ERROR INVALID FORMAT] "); askForFilterAge(prompt)
    }
  }

  def askForFilterBrands() : Map[Int, Set[String]] = {
    println("List brands name (using SEMI-COLON separator):")
    val brands = StdIn.readLine()
    brands
	.split(";")
	.map(x => x.toLowerCase)
	.groupBy(x => x.count(_ == ' '))
	.map(x => (x._1, x._2.toSet))
  }

  def welcome(user : User) : Unit = {
    println("\n\nWelcome " + user.firstname + " " + user.lastname + " alias " + user.username)
  }

}
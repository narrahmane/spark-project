package epita

object ModuleProducer
{

  import java.util.{Date, Properties}
  import org.apache.kafka.clients.producer.{ProducerConfig, KafkaProducer, ProducerRecord}
  import org.apache.kafka.common.serialization.{Serde, Serdes, StringDeserializer, StringSerializer}
  import epita.ModuleDatastructures._
  import epita.ModuleSerdes.CustomSerializer

  class ProducerAPI(brokers: String, clientId: String) {

    val topic_user = "topic-users"
    val topic_post = "topic-posts"
    val topic_message = "topic-messages"

    val userProducer = initUserProducer(brokers, clientId)
    val postProducer = initPostProducer(brokers, clientId)
    val messageProducer = initMessageProducer(brokers, clientId)

    def createDefaultConfig(brokers: String, clientId: String) : Properties = {
      val props = new Properties()
      props.put(ProducerConfig.CLIENT_ID_CONFIG, clientId)
      props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers)
      props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer])
      props
    }

    def initUserProducer(brokers: String, clientId: String) : KafkaProducer[String, User] = {
      val props = createDefaultConfig(brokers, clientId)
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[CustomSerializer[User]])
      new KafkaProducer[String, User](props)
    }

    def initPostProducer(brokers: String, clientId: String) : KafkaProducer[String, Post] = {
      val props = createDefaultConfig(brokers, clientId)
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[CustomSerializer[Post]])
      new KafkaProducer[String, Post](props)
    }

    def initMessageProducer(brokers: String, clientId: String) : KafkaProducer[String, Message] = {
      val props = createDefaultConfig(brokers, clientId)
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[CustomSerializer[Message]])
      new KafkaProducer[String, Message](props)
    }

    def sendUser(user: User) : Unit = {
      val data = new ProducerRecord[String, User](topic_user, user.username, user)
      userProducer.send(data)
    }

    def sendPost(post: Post) : Unit = {
      val data = new ProducerRecord[String, Post](topic_post, post.username, post)
      postProducer.send(data)
    }

    def sendMessage(msg: Message) : Unit = {
      val data = new ProducerRecord[String, Message](topic_message, msg.sender, msg)
      messageProducer.send(data)
    }

    def shutdown() : Unit = {
      userProducer.close()
      postProducer.close()
      messageProducer.close()
    }
  }
}
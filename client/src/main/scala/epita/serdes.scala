package epita

object ModuleSerdes {

  import ModuleDatastructures._
  import java.io.{ObjectInputStream, ByteArrayInputStream}
  import java.util

  import org.apache.kafka.common.serialization.{Deserializer, Serializer}

  class CustomDeserializer[T] extends Deserializer[T] {

    override def configure(configs: util.Map[String,_],isKey: Boolean):Unit = {

    }

    override def deserialize(topic:String,bytes: Array[Byte]) = {
      val byteIn = new ByteArrayInputStream(bytes)
      val objIn = new ObjectInputStream(byteIn)
      val obj = objIn.readObject().asInstanceOf[T]
      byteIn.close()
      objIn.close()
      obj
    }

    override def close():Unit = {

    }

  }

  import java.io.{ObjectOutputStream, ByteArrayOutputStream}
  import java.util
  import org.apache.kafka.common.serialization.Serializer


  class CustomSerializer[T] extends Serializer[T]{

    override def configure(configs: util.Map[String,_],isKey: Boolean):Unit = {

    }

    override def serialize(topic:String, data:T):Array[Byte] = {
      try {
        val byteOut = new ByteArrayOutputStream()
        val objOut = new ObjectOutputStream(byteOut)
        objOut.writeObject(data)
        objOut.close()
        byteOut.close()
        byteOut.toByteArray
      }
      catch {
        case ex:Exception => throw new Exception(ex.getMessage)
      }
    }

    override def close():Unit = {

    }

  }

}
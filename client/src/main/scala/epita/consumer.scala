package epita

object ModuleConsumer {

  import epita.ModuleDatastructures._
  import epita.ModuleOutput._
  import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer, ConsumerRecord}
  import org.apache.kafka.common.serialization._
  import epita.ModuleSerdes.{CustomDeserializer}
  import org.apache.spark._
  import org.apache.spark.streaming._
  import org.apache.spark.streaming.kafka010._
  import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
  import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
  import org.apache.hadoop.mapred._

  class ConsumerAPI(hdfsPath : String) {

    val sparkConf = new SparkConf()
        .setAppName("Console Producer-Consumer API")
        .setMaster("local[*]")
    val ssc = new StreamingContext(sparkConf, Seconds(5))

    def benchmark(dateFrom: Long, dateTo: Long, ageFrom: Int, ageTo: Int, brands: Map[Int, Set[String]]) : Unit = {
      val benchPosts = benchmarkPosts(dateFrom, dateTo, ageFrom, ageTo, brands)
      val benchMessages = benchmarkMessages(dateFrom, dateTo, ageFrom, ageTo, brands)
      val totalMeanAge = (benchPosts._3, benchMessages._3) match {
      	  	       	   case (0, 0) => 0
			   case (0, y) => y
			   case (x, 0) => x
			   case (x, y) => (x + y) / 2
			 }
      printBenchmark(benchPosts._1 + benchMessages._1,
      		     benchPosts._2.union(benchMessages._2).size,
		     totalMeanAge,
		     brands)
    }

    def benchmarkPosts(dateFrom: Long, dateTo: Long, ageFrom: Int, ageTo: Int, brands: Map[Int, Set[String]]) : (Int, Set[String], Int) = {
      val data = receiveDPosts(dateFrom, dateTo, ageFrom, ageTo, brands)
      val dataByUser =  data.groupBy(_.user.username)
      val nbPost = data.size
      val nbUser = dataByUser.size
      val meanAge = nbUser match { case 0 => 0
      	  	      	           case x => (dataByUser.foldLeft(0){(acc, arrDpost) => acc + arrDpost._2.head.user.age} / nbUser)
			    	 }

      printBenchmarkPosts(nbPost, nbUser, meanAge, brands)

      (nbPost, dataByUser.map(_._1).toSet, meanAge)
    }

    def benchmarkMessages(dateFrom: Long, dateTo: Long, ageFrom: Int, ageTo: Int, brands: Map[Int, Set[String]]) : (Int, Set[String], Int) = {
      val data = receiveDMessages(dateFrom, dateTo, ageFrom, ageTo, brands)
      val dataBySender = data.groupBy(_.sender.username)
      val nbMessage = data.size
      val nbSender = dataBySender.size
      val meanAge = nbSender match { case 0 => 0
      	  	      	             case x => (dataBySender.foldLeft(0){(acc, arrDmessage) => acc + arrDmessage._2.head.sender.age} / nbSender)
			            }

      printBenchmarkMessages(nbMessage, nbSender, meanAge, brands)

      (nbMessage, dataBySender.map(_._1).toSet, meanAge)
    }

    def receiveDPosts(dateFrom: Long, dateTo: Long, ageFrom: Int, ageTo: Int, brands: Map[Int, Set[String]]) : Array[DenormalizedPost] = {
      try {
        val data = ssc.sparkContext.objectFile[(java.lang.Long, DenormalizedPost)](hdfsPath + "/save-dpost*/*")

	val result = data
	  .filter(tuple => tuple._1 >= dateFrom && tuple._1 <= dateTo
	  		   && tuple._2.user.age >= ageFrom && tuple._2.user.age <= ageTo)
	  .map(tuple => (tuple._2, tuple._2.post.text
						  .split(" ")
						  .map(x => x.toLowerCase)))

	brands
	  .map(entry => result.map(tuple => (tuple._1, tuple._2
	  	     			    	         .sliding(entry._1 + 1, 1)
							 .toList
	  	     			    	         .map(x => x.mkString(" "))
							 .toSet))
			      .filter(tuple => entry._2.subsetOf(tuple._2))
			      .map(_._1))
	  .map(x => x.collect)
	  .reduce((acc, x) => acc.intersect(x))
        } catch {
          case e: InvalidInputException => Array()
        }
    }

    def receiveDMessages(dateFrom: Long, dateTo: Long, ageFrom: Int, ageTo: Int, brands: Map[Int, Set[String]]) : Array[DenormalizedMessage] = {
      try {
        val data = ssc.sparkContext.objectFile[(java.lang.Long, DenormalizedMessage)](hdfsPath + "/save-dmessage*/*")

        val result = data
	  .filter(tuple => tuple._1 >= dateFrom && tuple._1 <= dateTo
	  		   && tuple._2.sender.age >= ageFrom && tuple._2.sender.age <= ageTo)
	  .map(tuple => (tuple._2, tuple._2.message.text
						   .split(" ")
						   .map(x => x.toLowerCase)))
	brands
	  .map(entry => result.map(tuple => (tuple._1, tuple._2
	  	     			    	         .sliding(entry._1 + 1, 1)
							 .toList
	  	     			    	         .map(x => x.mkString(" "))
							 .toSet))
			      .filter(tuple => entry._2.subsetOf(tuple._2))
			      .map(_._1))
	  .map(x => x.collect)
	  .reduce((acc, x) => acc.intersect(x))
        } catch {
          case e: InvalidInputException => Array()
        }
    }

  }

}
package epita

object ModuleDatastructures {

  case class Message(sender: String, receiver: String, text: String, timestamp: Long)

  case class Post(username: String, text: String, timestamp: Long)

  case class User(username: String, firstname: String, lastname: String, age: Int, timestamp: Long)

  case class DenormalizedPost(post: Post, user: User)

  case class DenormalizedMessage(sender: User, receiver: User, message: Message)
}
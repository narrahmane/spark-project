package epita

object ModuleOutput {

  def printBenchmark(nbContent: Int, nbAuthors: Int, meanAge: Int, brands: Map[Int, Set[String]]) {
    println("\t\tBENCHMARK RECAP")
    print("Brands: ")
    brands.map(_._2.map(x => print(x + ";")))
    println("\n")
    println("Total content found: " + nbContent)
    println("Total unique authors found: " + nbAuthors)
    println("Total average age: " + meanAge)
    println("\n")
  }

  def printBenchmarkPosts(nbPost: Int, nbUser: Int, meanAge: Int, brands: Map[Int, Set[String]]) {
    println("\t\tBENCHMARK OF POSTS ")
    print("Brands: ")
    brands.map(_._2.map(x => print(x + ";")))
    println("\n")
    println("Number found: " + nbPost)
    println("Number of unique user: " + nbUser)
    println("Average age: " + meanAge)
    println("\n");
  }

  def printBenchmarkMessages(nbMessage: Int, nbSender: Int, meanAge: Int, brands: Map[Int, Set[String]]) {
    println("\t\tBENCHMARK OF MESSAGES")
    print("Brands: ")
    brands.map(_._2.map(x => print(x + ";")))
    println("\n")
    println("Number found: " + nbMessage)
    println("Number of unique sender: " + nbSender)
    println("Average age: " + meanAge)
    println("\n");
  }

}
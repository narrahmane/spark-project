###################### DEPENDENCIES #######################

Scala 2.11
Kafka kafka_2.11-1.1.0
JAVA 8

###################### SETUP #######################

client/   	 : use to run a client as interactive mode to produce
			   data inside kafka topics and get data from HDFS trough
			   a brand names benchmark tool.
services/ 	 : spark streaming micro service which aggregates data
			   from kafka topic.
hdfs-writer/ : spark streaming app providing the ability to persists
			   data inside HDFS

###################### REQUIREMENTS #######################

Environment variables:
	KAFKA_BIN_PATH=path/to/kafka/bin
	KAFKA_CONFIG_PATH=path/to/kafka/config
	ZOO_ADDR=zookeeperAdress:PORT

3 Kafka Brokers configs as below:
	KAFKA_CONFIG_PATH/server.properties
	KAFKA_CONFIG_PATH/server-1.properties
	KAFKA_CONFIG_PATH/server-2.properties

###################### RUN #######################

Terminal_1> ./deploy.sh
Terminal_2> cd services
			sbt "run brokerAddr"
Terminal_3> cd hdfs-writer
			sbt "run path/to/hdfs brokerAddr groupId"
Terminal_4> cd client
			sbt "run brokerAddr producerId path/to/hdfs"

####################

Authors:

Timothee Brenet (brenet_t)
Nacim Arrahmane (arrahm_n)

###########################################################
package epita

object ModuleSerdes {

  import java.io.{ObjectInputStream, ByteArrayInputStream}
  import java.util
  import epita.ModuleDatastructures._

  import org.apache.kafka.common.serialization.{Deserializer, Serializer}

  class CustomDeserializer[T >: Null] extends Deserializer[T] {

    override def configure(configs: util.Map[String,_],isKey: Boolean):Unit = {

    }

    override def deserialize(topic:String, bytes: Array[Byte]) = {
      println("[ServiceDeserializer] topic=" + topic + " array[Byte]=" + bytes)
      if (bytes == null)
        null
      else {
        val byteIn = new ByteArrayInputStream(bytes)
        val objIn = new ObjectInputStream(byteIn)
        val obj = objIn.readObject().asInstanceOf[T]
        byteIn.close()
        objIn.close()
        obj
      }
    }

    override def close():Unit = {

    }

  }
/*
  class CustomDeserializerUser extends Deserializer[User] {

    override def configure(configs: util.Map[String,_],isKey: Boolean):Unit = {

    }

    override def deserialize(topic:String,bytes: Array[Byte]) = {
      println("[ServiceDeserializer] topic=" + topic + " array[Byte]=" + bytes)
      if (bytes == null)
        null
      else {
        val byteIn = new ByteArrayInputStream(bytes)
        val objIn = new ObjectInputStream(byteIn)
        val obj = objIn.readObject().asInstanceOf[User]
        byteIn.close()
        objIn.close()
        obj
      }
    }
    override def close():Unit = {

    }

  }
*/
  import java.io.{ObjectOutputStream, ByteArrayOutputStream}
  import java.util
  import org.apache.kafka.common.serialization.Serializer


  class CustomSerializer[T] extends Serializer[T]{

    override def configure(configs: util.Map[String,_],isKey: Boolean):Unit = {

    }

    override def serialize(topic:String, data:T):Array[Byte] = {
      try {
        println("[ServiceSerializer] topic=" + topic + " data=" + data)
        val byteOut = new ByteArrayOutputStream()
        val objOut = new ObjectOutputStream(byteOut)
        objOut.writeObject(data)

        objOut.close()
        byteOut.close()
        byteOut.toByteArray
      }
      catch {
        case ex:Exception => println("[ServiceSerializer] Exception catch"); throw new Exception(ex.getMessage)
      }
    }

    override def close():Unit = {

    }

  }

}
package epita

object Writer {

  import epita.ModuleDatastructures._
  import epita.ModuleSerdes.{CustomDeserializer}
  import org.apache.spark._
  import org.apache.spark.streaming._
  import org.apache.spark.streaming.kafka010._
  import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
  import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
  import org.apache.kafka.clients.consumer.{ConsumerConfig, KafkaConsumer, ConsumerRecord}
  import org.apache.kafka.common.serialization._

  def main(args: Array[String]) : Unit = {
	args.length match {
		case x if x < 3 => println("Not enough arguments")
		case x if x >= 3 => process(args)
	}
  }

  def process(args : Array[String]) : Unit = {
    val topic_timestampDpost = "topic-timestampDpost"
    val topic_timestampDmessage = "topic-timestampDmessage"

    val path = args(0)
    val path_dpost = path + "/save-dpost"
    val path_dmessage = path + "/save-dmessage"

    val sparkConf = new SparkConf()
        .setAppName("HDFS Writer")
        .setMaster("local[*]")
    val ssc = new StreamingContext(sparkConf, Seconds(2))
    val brokerList = args.slice(1, args.length - 1).mkString(",")
    println(brokerList)

    def createDefaultConfig(valueDeser: Object, keyDeser: Object) = Map[String, Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> brokerList,
      ConsumerConfig.GROUP_ID_CONFIG -> args(args.length - 1),
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> keyDeser,
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> valueDeser,
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "latest",
      ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG -> (false: java.lang.Boolean)
      )

      val dpostTimestampProps = createDefaultConfig(classOf[CustomDeserializer[DenormalizedPost]], classOf[LongDeserializer])
      val stream_dpost = KafkaUtils.createDirectStream[Long, DenormalizedPost] (
        ssc,
        PreferConsistent,
        Subscribe[Long, DenormalizedPost](Array(topic_timestampDpost), dpostTimestampProps)
      )
      stream_dpost.map(x => (x.key, x.value)).saveAsObjectFiles(path_dpost, "backup");

      val dmessageTimestampProps = createDefaultConfig(classOf[CustomDeserializer[DenormalizedMessage]], classOf[LongDeserializer])
      val stream_dmessage = KafkaUtils.createDirectStream[Long, DenormalizedMessage] (
        ssc,
        PreferConsistent,
        Subscribe[Long, DenormalizedMessage](Array(topic_timestampDmessage), dmessageTimestampProps)
      )
      stream_dmessage.map(x => (x.key, x.value)).saveAsObjectFiles(path_dmessage, "backup");

      ssc.start()
      ssc.awaitTermination()
  }

}
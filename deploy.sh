#! /bin/bash

# Topics where data are received into kafka
TOPIC_USERS="topic-users"
TOPIC_POSTS="topic-posts"
TOPIC_MESSAGES="topic-messages"
TOPIC_DPOSTS="topic-timestampDpost"
TOPIC_DMESSAGES="topic-timestampDmessage"

# List available options
help() {
    info="./commands.sh\n\t[--start]\
                       \n\t[--stop]\
                       \n\t[--create-topics name1 [name2 ...]]\
                       \n\t[--list-topics]\
                       \n\t[--list-brokers]"

    echo -e "$info"
    exit
}

# Create topics
create_topics() {
    while [ $# -ne 0 ]; do
        $KAFKA_BIN_PATH/kafka-topics.sh --create --zookeeper $ZOO_ADDR --replication-factor 1 --partitions 1 --topic "$1" &
        shift
    done
}

# Initialize users, posts & messages topics
init_topics() {
    create_topics "$TOPIC_USERS" &
    create_topics "$TOPIC_POSTS" &
    create_topics "$TOPIC_MESSAGES" &
    create_topics "$TOPIC_DPOSTS" &
    create_topics "$TOPIC_DMESSAGES"
}

# List topics available
list_topics() {
    $KAFKA_BIN_PATH/kafka-topics.sh --list --zookeeper $ZOO_ADDR
}

# List kafka brokers connected
list_brokers() {
    echo "ls /brokers/ids" | $KAFKA_BIN_PATH/zookeeper-shell.sh $ZOO_ADDR
}

# Start zookeeper & kafka brokers instances
start() {
    bash $KAFKA_BIN_PATH/zookeeper-server-start.sh $KAFKA_CONFIG_PATH/zookeeper.properties &
    sleep 5
    bash $KAFKA_BIN_PATH/kafka-server-start.sh $KAFKA_CONFIG_PATH/server.properties  &
    sleep 5
    bash $KAFKA_BIN_PATH/kafka-server-start.sh $KAFKA_CONFIG_PATH/server-1.properties &
    sleep 5
    bash $KAFKA_BIN_PATH/kafka-server-start.sh $KAFKA_CONFIG_PATH/server-2.properties &
    sleep 5
    init_topics &
}

# Stop zookeeper & kafka brokers instances
stop() {
    bash $KAFKA_BIN_PATH/kafka-server-stop.sh &
    sleep 5
    bash $KAFKA_BIN_PATH/kafka-server-stop.sh &
    sleep 5
    bash $KAFKA_BIN_PATH/kafka-server-stop.sh &
    sleep 5
    bash $KAFKA_BIN_PATH/zookeeper-server-stop.sh &
}


if [ $# -eq 0 ]; then
    help
 fi

# Check environment variables
if [ -z "$KAFKA_BIN_PATH" ]; then
    echo "set KAFKA_BIN_PATH"
    exit
fi
if [ -z "$KAFKA_CONFIG_PATH" ]; then
    echo "set KAFKA_CONFIG_PATH"
    exit
fi
if [ -z "$ZOO_ADDR" ]; then
    echo "set ZOO_ADDR zookeeper adress"
    exit
fi

# Parse options
while [ $# -ne 0 ]; do
    key="$1"
    case $key in
        "--start")
	    start
            ;;
        "--stop")
            stop
            ;;
        "--create-topics")
            shift
            create_topics "$@"
            ;;
        "--list-topics")
            list_topics
            ;;
        "--list-brokers")
            list_brokers
            ;;
        *)
            help
            ;;
    esac
    shift
done
